package cedeporunners.mainpack.deporunners.Activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import cedeporunners.mainpack.deporunners.Fragments.CarneFragment;
import cedeporunners.mainpack.deporunners.Fragments.PasswordFragment;
import cedeporunners.mainpack.deporunners.Fragments.SponsorsFragment;
import cedeporunners.mainpack.deporunners.POJO.Socio;
import cedeporunners.mainpack.deporunners.R;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static String SOCIO_KEY = "socio";

    private DrawerLayout drawer;
    private Socio socio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);

        socio = (Socio) getIntent().getExtras().get(SOCIO_KEY);
        SharedPreferences preferences = getSharedPreferences("data", MODE_PRIVATE);
        preferences.edit().putInt("socio_id", socio.getId()).apply();

        drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(android.R.color.white));

        NavigationView nav_view = findViewById(R.id.nav_view);

        View header = nav_view.getHeaderView(0);

        TextView nav_name = header.findViewById(R.id.nav_socioName);
        TextView nav_email = header.findViewById(R.id.nav_socioEmail);

        nav_name.setText(socio.getNom() + " " + socio.getCognoms());
        nav_email.setText(socio.getEmail());

        nav_view.setNavigationItemSelectedListener(this);

        nav_view.getMenu().performIdentifierAction(R.id.nav_carnet, 1);
        nav_view.setCheckedItem(R.id.nav_carnet);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Bundle data = new Bundle();
        switch (menuItem.getItemId()) {
            case R.id.nav_carnet:
                CarneFragment carneFragment = new CarneFragment();
                data.putParcelable(SOCIO_KEY, socio);
                carneFragment.setArguments(data);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, carneFragment).commit();
                break;

            case R.id.nav_password:
                PasswordFragment passwordFragment = new PasswordFragment();
                data.putParcelable(SOCIO_KEY, socio);
                passwordFragment.setArguments(data);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, passwordFragment).commit();
                break;

            case R.id.nav_sponsors:
                SponsorsFragment sponsorFragment = new SponsorsFragment();
                data.putParcelable(SOCIO_KEY, socio);
                sponsorFragment.setArguments(data);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, sponsorFragment).commit();
                break;

            case R.id.nav_logout:
                dialogLogout();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }

    private void dialogLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Vols tancar la sessió?")
                .setPositiveButton(Html.fromHtml("<font color='#000000'>Sí</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences preferences = getSharedPreferences("data", MODE_PRIVATE);
                        preferences.edit().putInt("isLogged", 0).apply();
                        preferences.edit().putInt("socio_id", 0).apply();
                        finish();
                    }
                })
                .setNegativeButton(Html.fromHtml("<font color='#000000'>No</font>"), null)
                .setIcon(R.drawable.ic_exit);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
