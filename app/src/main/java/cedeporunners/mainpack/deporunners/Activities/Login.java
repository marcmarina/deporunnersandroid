package cedeporunners.mainpack.deporunners.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import cedeporunners.mainpack.deporunners.R;

import cedeporunners.mainpack.deporunners.API.API;
import cedeporunners.mainpack.deporunners.API.Services.SocioService;
import cedeporunners.mainpack.deporunners.POJO.Socio;
import cedeporunners.mainpack.deporunners.Threads.LoginProcess;
import cedeporunners.mainpack.deporunners.Utils.StringHashing;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences sharedPref = getSharedPreferences("data", MODE_PRIVATE);
        int number = sharedPref.getInt("isLogged", 0);
        if(number == 1) {
            LoginProcess loginP = new LoginProcess(sharedPref.getInt("socio_id", 0));
            loginP.start();
            try {
                loginP.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Socio s = loginP.getSocio();

            if(s != null) {
                loginSuccessful(s);
            }
        }

        CardView login = findViewById(R.id.btnLogin);
        final EditText usernameBox = findViewById(R.id.txtBoxUsername);
        final EditText passwordBox = findViewById(R.id.txtBoxPassword);
        ImageView revealPassword = findViewById(R.id.revealPassword);

        revealPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    passwordBox.setInputType(InputType.TYPE_CLASS_TEXT);
                    passwordBox.setSelection(passwordBox.getText().length());
                    passwordBox.setTypeface(Typeface.DEFAULT);
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    passwordBox.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordBox.setSelection(passwordBox.getText().length());
                    passwordBox.setTypeface(Typeface.DEFAULT);
                }
                return true;
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameBox.getText().toString();
                String password = StringHashing.hashString(passwordBox.getText().toString());
                login(username, password);
            }
        });

        final ConstraintLayout mainLayout = findViewById(R.id.mainLayout);

        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
            }
        });
    }

    public void login(String username, String password){
        SocioService sService = API.getApi().create(SocioService.class);
        Call<Socio> socioCall = sService.login(username, password);

        socioCall.enqueue(new Callback<Socio>() {
            @Override
            public void onResponse(Call<Socio> call, Response<Socio> response) {
                if(response.body() != null) {
                    loginSuccessful(response.body());
                } else {
                    Toast.makeText(Login.this, "Usuari i/o contrasenya incorrecte", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Socio> call, Throwable t) {
//                Toast.makeText(Login.this, t.toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(Login.this, "No s'ha pogut connectar, intenta-ho més endavant", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loginSuccessful(Socio s) {
        SharedPreferences preferences = getSharedPreferences("data", MODE_PRIVATE);
        preferences.edit().putInt("isLogged", 1).apply();

        Intent i = new Intent(Login.this, MainActivity.class);
        i.putExtra(MainActivity.SOCIO_KEY, s);
        startActivity(i);
        finish();
    }
}
