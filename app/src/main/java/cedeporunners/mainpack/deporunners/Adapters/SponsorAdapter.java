package cedeporunners.mainpack.deporunners.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cedeporunners.mainpack.deporunners.POJO.Sponsor;
import cedeporunners.mainpack.deporunners.R;

public class SponsorAdapter extends RecyclerView.Adapter<SponsorAdapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<Sponsor> sponsors;
    private View.OnClickListener listener;

    public SponsorAdapter(ArrayList<Sponsor> sponsors) {
        this.sponsors = sponsors;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sponsor_item, viewGroup, false);
        view.setOnClickListener(this);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bindSponsor(sponsors.get(i));
    }

    @Override
    public int getItemCount() {
        return sponsors.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView sponsorName;
        private ImageView sponsorImg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //sponsorName = itemView.findViewById(R.id.sponsorName);
            sponsorImg = itemView.findViewById(R.id.sponsorImg);
        }

        public void bindSponsor(Sponsor s) {
            //sponsorName.setText(s.getName());
            sponsorImg.setImageResource(s.getImg());
        }
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }
}
