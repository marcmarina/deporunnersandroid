package cedeporunners.mainpack.deporunners.Managers;

import java.util.ArrayList;

import cedeporunners.mainpack.deporunners.POJO.Sponsor;
import cedeporunners.mainpack.deporunners.R;

public class SponsorManager {

    public static ArrayList<Sponsor> getSponsors() {
        ArrayList<Sponsor> sponsors = new ArrayList<>();

        sponsors.add(new Sponsor("Margalló", R.drawable.margallo, null));
        sponsors.add(new Sponsor("Crf Vilanova", R.drawable.crf, null));
        sponsors.add(new Sponsor("Calserra", R.drawable.calserra, null));
        sponsors.add(new Sponsor("Fisionova", R.drawable.fisionova, null));

        return sponsors;
    }

}
