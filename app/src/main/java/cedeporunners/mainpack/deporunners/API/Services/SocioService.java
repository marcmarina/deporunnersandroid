package cedeporunners.mainpack.deporunners.API.Services;

import java.util.List;

import cedeporunners.mainpack.deporunners.POJO.Socio;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SocioService {
    @GET("api/socios")
    Call<List<Socio>> getSocios();

    @GET("api/socios/{id}")
    Call<Socio> getSocio(@Path("id") int id);

    @GET("api/sociologin")
    Call<Socio> login(@Query("username") String username, @Query("password") String password);

    @PUT("api/sociopassword")
    Call<ResponseBody> changePassword(@Query("id") int id, @Query("old_pw") String old_pw, @Query("new_pw") String new_pw);
}
