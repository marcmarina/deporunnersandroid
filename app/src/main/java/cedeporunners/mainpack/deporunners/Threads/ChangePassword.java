package cedeporunners.mainpack.deporunners.Threads;

import cedeporunners.mainpack.deporunners.API.API;
import cedeporunners.mainpack.deporunners.API.Services.SocioService;
import cedeporunners.mainpack.deporunners.POJO.Socio;

import java.io.IOException;

public class ChangePassword extends Thread {
    private Socio socio;
    private String old_password;
    private String new_password;
    private int code;

    public ChangePassword(Socio socio, String old_password, String new_password) {
        this.socio = socio;
        this.old_password = old_password;
        this.new_password = new_password;
    }

    @Override
    public void run() {
        SocioService service = API.getApi().create(SocioService.class);
        try {
            code = service.changePassword(socio.getId(), old_password, new_password).execute().code();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getCode() {
        return code;
    }
}
