package cedeporunners.mainpack.deporunners.Threads;

import java.io.IOException;

import cedeporunners.mainpack.deporunners.API.API;
import cedeporunners.mainpack.deporunners.API.Services.SocioService;
import cedeporunners.mainpack.deporunners.POJO.Socio;
import retrofit2.Call;

public class LoginProcess extends Thread {
    private Socio socio;
    private int socio_id;

    public LoginProcess(int socio_id) {
        this.socio_id = socio_id;
    }

    @Override
    public void run() {
        SocioService service = API.getApi().create(SocioService.class);

        Call<Socio> getSocioID = service.getSocio(socio_id);

        try {
            socio = getSocioID.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socio getSocio() {
        return socio;
    }
}
