package cedeporunners.mainpack.deporunners.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import cedeporunners.mainpack.deporunners.Activities.MainActivity;
import cedeporunners.mainpack.deporunners.Adapters.SponsorAdapter;
import cedeporunners.mainpack.deporunners.Managers.SponsorManager;
import cedeporunners.mainpack.deporunners.POJO.Socio;
import cedeporunners.mainpack.deporunners.POJO.Sponsor;
import cedeporunners.mainpack.deporunners.R;


public class SponsorsFragment extends Fragment {
    View v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_sponsors, container, false);

        Bundle data = this.getArguments();
        if(data != null) {
            Socio socio = (Socio) data.get(MainActivity.SOCIO_KEY);
            fillSponsors(SponsorManager.getSponsors());
        }

        return v;
    }

    private void fillSponsors(ArrayList<Sponsor> sponsors) {
        RecyclerView listRec = v.findViewById(R.id.all_sponsor_list);

        SponsorAdapter adapter = new SponsorAdapter(sponsors);
        listRec.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        listRec.setAdapter(adapter);


    }
}
