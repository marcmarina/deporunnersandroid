package cedeporunners.mainpack.deporunners.Fragments;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import cedeporunners.mainpack.deporunners.Activities.MainActivity;
import cedeporunners.mainpack.deporunners.POJO.Socio;
import cedeporunners.mainpack.deporunners.QR.Contents;
import cedeporunners.mainpack.deporunners.QR.QRCodeEncoder;
import cedeporunners.mainpack.deporunners.R;

import static android.content.Context.WINDOW_SERVICE;


public class CarneFragment extends Fragment {
    View v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_carne, container, false);

        Bundle data = this.getArguments();
        if(data != null) {
            Socio socio = (Socio) data.get(MainActivity.SOCIO_KEY);
            llenarDatos(socio);
        }

        return v;
    }

    private void llenarDatos(Socio s) {
        TextView numSocio = v.findViewById(R.id.numSocio);
        TextView name = v.findViewById(R.id.socioName);
        TextView dni = v.findViewById(R.id.socioDni);

        numSocio.setText(String.format("%03d", Integer.parseInt(s.getNum_socio())));
        name.setText(s.getNom() + " " + s.getCognoms());
        dni.setText(s.getDni());

        WindowManager manager = (WindowManager) getActivity().getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3/4;

        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(s.toString(),
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                smallerDimension);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            ImageView myImage = v.findViewById(R.id.socioQR);
            myImage.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
}
