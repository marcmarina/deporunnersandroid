package cedeporunners.mainpack.deporunners.Fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import cedeporunners.mainpack.deporunners.Activities.MainActivity;
import cedeporunners.mainpack.deporunners.POJO.Socio;
import cedeporunners.mainpack.deporunners.R;
import cedeporunners.mainpack.deporunners.Threads.ChangePassword;
import cedeporunners.mainpack.deporunners.Utils.StringHashing;

public class PasswordFragment extends Fragment {
    private View v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_contrasenya, container, false);

        Bundle data = getArguments();
        final Socio socio = (Socio) data.get(MainActivity.SOCIO_KEY);

        final EditText old_pw = v.findViewById(R.id.old_password);
        final EditText new_pw = v.findViewById(R.id.new_password);
        final EditText new_pw_confirm = v.findViewById(R.id.new_password_confirm);

        CardView apply = v.findViewById(R.id.changePassword);

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!new_pw_confirm.getText().toString().equals(new_pw.getText().toString())) {
                    Toast.makeText(getContext(), "Les contrasenyes no coincideixen", Toast.LENGTH_SHORT).show();
                } else {
                    ChangePassword changePassword = new ChangePassword(
                            socio,
                            StringHashing.hashString(old_pw.getText().toString()),
                            StringHashing.hashString(new_pw.getText().toString())
                    );

                    changePassword.start();
                    try {
                        changePassword.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (changePassword.getCode() == 200) {
                        Toast.makeText(getContext(), "Canvis desats correctament", Toast.LENGTH_SHORT).show();
                        old_pw.setText("");
                        new_pw.setText("");
                        new_pw_confirm.setText("");
                    } else if (changePassword.getCode() == 404) {
                        Toast.makeText(getContext(), "La contrasenya es incorrecte", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "No s'han pogut desar els canvis", Toast.LENGTH_SHORT).show();
                        old_pw.setText("");
                        new_pw.setText("");
                        new_pw_confirm.setText("");
                    }
                }
            }
        });

        ImageView reveal = v.findViewById(R.id.revealAllPasswords);
        reveal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    old_pw.setInputType(InputType.TYPE_CLASS_TEXT);
                    old_pw.setSelection(old_pw.getText().length());
                    old_pw.setTypeface(Typeface.DEFAULT);

                    new_pw.setInputType(InputType.TYPE_CLASS_TEXT);
                    new_pw.setSelection(new_pw.getText().length());
                    new_pw.setTypeface(Typeface.DEFAULT);

                    new_pw_confirm.setInputType(InputType.TYPE_CLASS_TEXT);
                    new_pw_confirm.setSelection(new_pw_confirm.getText().length());
                    new_pw_confirm.setTypeface(Typeface.DEFAULT);
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    old_pw.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    old_pw.setSelection(old_pw.getText().length());
                    old_pw.setTypeface(Typeface.DEFAULT);

                    new_pw.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    new_pw.setSelection(new_pw.getText().length());
                    new_pw.setTypeface(Typeface.DEFAULT);

                    new_pw_confirm.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    new_pw_confirm.setSelection(new_pw_confirm.getText().length());
                    new_pw_confirm.setTypeface(Typeface.DEFAULT);
                }
                return true;
            }
        });

        return v;
    }
}
