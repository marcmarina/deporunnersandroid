package cedeporunners.mainpack.deporunners.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;

public class Socio implements Parcelable {
    private int id;
    private String nom, cognoms;
    private String dni;
    private String email;
    private String num_socio;

    protected Socio(Parcel in) {
        id = in.readInt();
        nom = in.readString();
        cognoms = in.readString();
        dni = in.readString();
        email = in.readString();
        num_socio = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nom);
        dest.writeString(cognoms);
        dest.writeString(dni);
        dest.writeString(email);
        dest.writeString(num_socio);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Socio> CREATOR = new Creator<Socio>() {
        @Override
        public Socio createFromParcel(Parcel in) {
            return new Socio(in);
        }

        @Override
        public Socio[] newArray(int size) {
            return new Socio[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public String getDni() {
        return dni;
    }

    public String getEmail() {
        return email;
    }

    public String getNum_socio() {
        return num_socio;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatEU = new SimpleDateFormat("dd/MM/yyyy");
        StringBuilder r = new StringBuilder();

        r.append("ID Soci: ").append(String.format("%03d", id));
        r.append("\nNom: ").append(nom).append(" ").append(cognoms);
        r.append("\nEmail: ").append(email);
        //r.append("\nData de naixement: ").append(formatEU.format(fechaNacimiento));
        //r.append("\nNum. Telefon: ").append(numTelefono);

        return r.toString();
    }

    public Socio() {
    }

}
