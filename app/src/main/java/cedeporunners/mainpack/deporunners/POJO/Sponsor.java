package cedeporunners.mainpack.deporunners.POJO;

public class Sponsor {

    private String name;
    private int img;
    private String website;

    public Sponsor(String name, int img, String website) {
        this.name = name;
        this.img = img;
        this.website = website;
    }

    public String getName() {
        return name;
    }

    public int getImg() {
        return img;
    }

    public String getWebsite() {
        return website;
    }
}
