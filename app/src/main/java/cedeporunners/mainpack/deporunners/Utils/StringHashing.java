package cedeporunners.mainpack.deporunners.Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StringHashing {
    public static String hashString(String raw) {
        String result = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encoded = digest.digest(
                    raw.getBytes()
            );
            result = bytesToHex(encoded);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}

